'use strict'
const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload
const sass = require('gulp-sass')
const rename = require('gulp-rename')
const autoprefixer = require('gulp-autoprefixer')
const cssmin = require('gulp-cssmin')
const mediaq = require('gulp-group-css-media-queries')
const iconfontCss = require('gulp-iconfont-css')
const iconfont = require('gulp-iconfont')
const runTimestamp = Math.round(Date.now()/1000)

gulp.task('server', () => {
	browserSync.init({
		server: {
			baseDir: './dist/'
		},
		open: "external",
		ghostMode: {
			clicks: false,
			forms: false,
			scroll: false
		}
	})
})
var fontName = 'icon';
gulp.task('icon', function(){
  	gulp.src(['./src/icons/*.svg'])
	.pipe(iconfontCss({
		fontName: fontName,
		targetPath: 'icon.scss',
		fontPath: '#{$icon_rute}'
	}))
	.pipe(iconfont({
		fontName: fontName,
		prependUnicode: true,
      	formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
		  timestamp: runTimestamp,
		  fontHeight: 1001
	}))
	.pipe(gulp.dest('./dist/fonts/iconfonts'));
})
gulp.task('assets', () => {
	return gulp.src('./src/scss/main.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['> 5%', 'ie 8'] }))
	.pipe(mediaq())
	.pipe(cssmin())
	.pipe(rename({suffix: '.min',}))
	.pipe(gulp.dest('./dist/css/'))
	.pipe(browserSync.stream())
})
//WATCH
gulp.task('watch', () => {
	gulp.watch('./dist/*.html').on('change', reload)
	gulp.watch('./dist/js/*.js').on('change', reload)
	gulp.watch('./src/scss/*.scss', ['assets'])
	gulp.watch('./src/scss/**/*.scss', ['assets'])
	gulp.watch('./src/scss/**/**/*.scss', ['assets'])
})
gulp.task('default', ['server', 'assets', 'watch'])
gulp.task('icons', ['icon'])