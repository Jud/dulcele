# Dulcele


| **Dependency** | **Description** | **Version** | **License** | **Type** |
| -------------- | --------------- | ----------- | ----------- | -------- |
 | [whirl@^1.0.0](https://github.com/jh3y/whirl) | Pure css loading animations with minimal effort! | 1.0.0 | MIT | dev | 
 | [uikit@3.0.0-rc.23](https://github.com/uikit/uikit/tree/v3.0.0-rc.23) | A lightweight and modular front-end framework for developing fast and powerful web interfaces | 3.0.0-rc.17 | MIT | dev 