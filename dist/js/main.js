function cover_fit(selector){
    var elements = document.querySelectorAll(selector);
    Array.prototype.forEach.call(elements, function(el, i){
        var parent_proportion = el.parentNode.offsetWidth / el.parentNode.offsetHeight;
        var image_proportion = el.getAttribute("width") / el.getAttribute("height");
        if(parent_proportion > image_proportion){
            el.classList.add('height-predominant');
        }
        else{
            el.classList.add('width-predominant');
        }
    });
}
function parent_fit(selector){
    var elements = document.querySelectorAll(selector);
    Array.prototype.forEach.call(elements, function(el, i){
        var parent_proportion = el.parentNode.getAttribute("width") / el.parentNode.getAttribute("height");
        var image_proportion = el.getAttribute("width") / el.getAttribute("height");
        if(parent_proportion > image_proportion){
            el.classList.add('height-predominant');
        }
        else{
            el.classList.add('width-predominant');
        }
    });
}
function image_cover_load(){
    var elements = document.querySelectorAll('*[uk-cover-async]');
    Array.prototype.forEach.call(elements, function(el, i){
        UIkit.cover(el, {});
        console.log(el);
    });
}
function move_desk_mobile(item, mobile, mq){
    if ( window.innerWidth  <= mq && ! document.querySelector(item).classList.contains('move-changed') ) {
        document.querySelector(mobile).appendChild(document.querySelector(item));
        document.querySelector(item).classList.add('move-changed');
    }
}
function back_mobile_desktop(item, desktop, mq){
    if ( window.innerWidth  > mq && document.querySelector(item).classList.contains('move-changed') ) {
        document.querySelector(desktop).appendChild(document.querySelector(item));
        document.querySelector(item).classList.remove('move-changed');
    }
}
function youtube_iframe(){
    var youtube = document.querySelectorAll( ".youtube" );
    // loop
    for (var i = 0; i < youtube.length; i++) { 
        youtube[i].addEventListener( "click", function() {
            var iframe = document.createElement( "iframe" );
                iframe.setAttribute( "frameborder", "0" );
                iframe.setAttribute( "allowfullscreen", "" );
                iframe.setAttribute( "src", "https://www.youtube.com/embed/"+ this.dataset.video +"?&rel=0&showinfo=0&autoplay=1&enablejsapi=1" );
                //borra el contenido interior this.innerHTML = "";
                //this.appendChild( iframe );
            $(this).children().append(iframe);
        } );
    }
}
document.addEventListener("DOMContentLoaded", function(){
    //--------------------------------
});
window.addEventListener('resize', function(){
    //--------------------------------
});
window.onload = function() {
    //--------------------------------
};